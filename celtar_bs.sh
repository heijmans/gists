#!/bin/bash

set -euf -o pipefail
. /etc/os-release
if [[ $VERSION_ID == "14.04" || $VERSION_ID == "16.04" ]]; then
	echo trusty/xenial
	SSH_SERVICE="service ssh"
	EXTRA_PKGS="linux-headers-generic"
elif [[ $VERSION == "7 (wheezy)" ]]; then
	echo wheezy
	SSH_SERVICE=/etc/init.d/ssh
	EXTRA_PKGS="linux-headers-amd64"
else
	echo unknown os-release
	exit -1
fi

cd
mkdir -p .ssh
touch .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
if grep -q FKbQXWWnyvngH .ssh/authorized_keys ; then echo ssh public key found ; else
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDH1EKS9sNGjTW2I2SiuC9nHPoEpxm1G19pAxU8iCS5zLH8sVxBAb0mYbtUHSR9QLSUaFu+oDjZvieo4U5YJ/dpZ7+Q622V2mXdPcqERjE9lAoGotXVbExB2Epob4i3beQwqeM1v2LFUbPEqC4Ou2S49cbuKW+cn2l8qjH9AZV+6rmlIt06RJZr9rw3ioZJbe5ajtAh5FKbQXWWnyvngH6p628ylDdaqXeyDGCXAs0i3GEkF0D0fqtFJD8uCKLt+I2uShy+MUA9uq8OPHxwTnzV0kzcdvNadS0cCmyV07zEFLJC1VKGupuQK+7LxeM/yJ0UGT9fUaKXrHjq8mXXpq4l jan@JaniMacOffice.local" >> .ssh/authorized_keys
fi

echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen en_US.UTF-8
echo LANG=en_US.UTF-8 > /etc/default/locale
echo LC_ALL=en_US.UTF-8 >> /etc/default/locale

apt-get -q -q -q -y update && apt-get -q -q -q -y upgrade && apt-get install -q -q -q -y aptitude
aptitude install -q -q -q -y man less rsync screen wget curl telnet attr anacron build-essential ntp vim-nox inetutils-ping git ruby ruby1.9.1 ruby-dev ruby1.9.1-dev openssh-server $EXTRA_PKGS

sed -i "s/Port 22/Port 8022/" /etc/ssh/sshd_config || echo "ssh port set"
$SSH_SERVICE restart

# move to celtar proper?
update-alternatives --set editor /usr/bin/vim.nox
git config --global user.name "Jan Heijmans"
git config --global user.email jan@egami.nl
git config --global push.default current

if grep -q rd/IpL8Le3pSBne8 .ssh/known_hosts ; then echo host key found ; else
	curl -sS https://bitbucket.org/heijmans/gists/raw/master/bb_known_hosts.txt >> .ssh/known_hosts
fi

if [[ ! -d celtar ]]; then
	git clone ssh://git@bitbucket.org:22/heijmans/celtar.git
fi

mkdir -p /etc/celtar
if [[ $# > 0 ]]; then
	echo $@ > /etc/celtar/recipes
fi

echo ======= CELTAR =======
cd celtar
./celtar.sh
echo ======= DONE =======
