#!/usr/bin/env python

import os
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

class RequestHandler(BaseHTTPRequestHandler):

  def do_GET(self):
    hostname = os.popen('hostname').read().strip()

    self.send_response(200)
    self.send_header("Content-Type", "text/plain")
    self.end_headers()
        
    wfile = self.wfile
    wfile.write("server: " + hostname + "\n")
    wfile.write("client: " + self.client_address[0] + "\n")
    wfile.write("\n");
    wfile.write(self.command + " " + self.path + " " + self.request_version + "\n")
    wfile.write(self.headers)
        
def main():
  server = HTTPServer(('', 80), RequestHandler)
  print('listening on :80')
  server.serve_forever()

main()


